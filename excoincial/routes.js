var user = require('./user');
var session = require('express-session');

module.exports = function (app) {

    //session
    app.get('/api', function (req, res) {
        if(req.session.page_views){
            req.session.page_views++;
            res.send("You visited this page " + req.session.page_views + " times");
        } else {
            req.session.page_views = 1;
            res.send("Welcome to this page for the first time!");
        }
    });

    // get requests
    app.get('/api/activate/:verifyCode', user.emailVerified)
    app.get('/api/login', user.login);

    //post requests
    app.post('/api/signup', user.signup);

    //put requests
    
};
