var database = require('./database');
var lib = require('./lib');
var sendEmail       = require('./sendEmail');

//email
function generateVerifyCode() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 20; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}



exports.signup = function(req,res,next)
{
    var param = [];
    param.email = lib.removeNullsAndTrim(req.body.email);
    param.password = lib.removeNullsAndTrim(req.body.password);
    param.verifyCode = generateVerifyCode();
    database.signup(param, function(res1){
        if(res1.status === 'Failed') res.json(res1);
        else {
            sendEmail.emailVerify(param.email, param.verifyCode, function(err, res2){
                if(err) res.json({status : 'Failed', msg : err.message});
                else {
                    res.json(res1);
                }
            });
        }
    });


};

exports.login = function(req,res,next)
{
    var param = [];
    param.email = lib.removeNullsAndTrim(req.query.email);
    param.password = lib.removeNullsAndTrim(req.query.password);

    database.login(param, function(result){
        res.json(result);
    })
};

exports.emailVerified  = function(req,res, next)
{
    var param = [];
    param.verifyCode = req.params.verifyCode;
    database.setEmailVerified(param, function(result){
        res.json(result);
    });

};
