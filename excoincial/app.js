var express = require('express');
var session = require('express-session');
var bodyParser = require('body-parser');
var routes = require('./routes');
var app = express();

app.set('port', process.env.port || 8080);

app.use(session({
    secret : "Shh, its a secret",
    resave : false,
    saveUnimitialized : true
}));

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});


app.use(bodyParser.json());


routes(app);
app.listen(app.get('port'), function(err, response){
    console.log('Server is running on port', app.get('port'));
});