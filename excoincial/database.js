const dbConnection = require('../config/dbconnection');
const passwordHash= require('password-hash');
const lib = require('./lib');
const connection = dbConnection();

exports.signup = function(param, callback){
   var password = param.password;
   console.log('SIGNUP : ', param);
   lib.log('SIGNUP : ', param.email + ' send signup request.');
   connection.query('Select * from users where email=?',[param.email],function(err, result){
       if(err) return callback({status : 'Failed', msg : err.message});
       if(result.length>0) return callback({status : 'Failed', msg: 'EXISTING USER'});
       var hashedPassword = passwordHash.generate(password);
       connection.query('insert into users set ?',{ email : param.email, password :  hashedPassword, verifyCode : param.verifyCode}, function(err1, result1){
           if(err1) return callback({status : 'Failed', msg : err1.message});
           return callback({status : 'Success', msg : result1});
       });
   });
};

exports.login = function(param, callback) {
    console.log('LOGIN : ', param);

    lib.log('LOGIN : ', param.email + ' send login request.');
    connection.query('Select * from users where email=?',[param.email],function(err, result){
        if(err)  return callback({status : 'Failed', msg : err.message});
        if(result.length === 0 ) return callback({status : 'Failed', msg : 'NO USER'});
        console.log(param.password, result[0].password);
        var verified = passwordHash.verify(param.password, result[0].password);
        if(!verified) return callback({status : 'Failed', msg : 'WRONG PASSWORD'});
        return callback({status : 'Success', msg : result[0]});
    })
};

exports.setEmailVerified = function(param, callback){

    console.log('EMAIL VERIFY : ', param);
    lib.log('EMAIL VERIFY : ', param.verifyCode);
    connection.query('select * from users where verifyCode=?',[param.verifyCode], function(err, result){
        if(err) return callback({status : 'Failed', msg  : err.message})
        if(result.length === 0) return callback({status : 'Failed', msg : 'VERIFYCODE IS NOT MATCHED'});
        connection.query('Update users Set email_verified = ?, verifyCode = ? where verifyCode = ?',[1, '', param.verifyCode],function(err, result){
            if(err) return callback({status : 'Failed', msg : err.message});
            return callback({status : 'Success', msg : result});
        })

    })

};
