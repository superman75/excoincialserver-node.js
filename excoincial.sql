DROP TABLE IF EXISTS users;
CREATE TABLE users (
 user_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
 email VARCHAR (255) NOT NULL,
 password VARCHAR (255) NOT NULL,
 created_at VARCHAR (255),
 last_at VARCHAR (255),
 birthMonth VARCHAR (20),
 birthDay INTEGER,
 birthYear INTEGER,
 country VARCHAR (50),
 street VARCHAR (255),
 aptNo VARCHAR (255),
 city VARCHAR (255),
 state VARCHAR (255),
 zipcode INTEGER,
 profile_completed BOOLEAN DEFAULT false,
 cardimgurl VARCHAR (255),
 email_verified BOOLEAN DEFAULT false,
 verifyCode VARCHAR(255),
 id_verified BOOLEAN DEFAULT false,
 twofact_verified BOOLEAN DEFAULT false,
 securitykey VARCHAR(255),
 phonenumber VARCHAR(50) UNIQUE
);

DROP TABLE IF EXISTS "orders";
CREATE TABLE "orders" (
 order_id uuid NOT NULL,
 user_id int4 NOT NULL,
 market VARCHAR(20) NOT NULL,
 order_type VARCHAR(20) NOT NULL,
 quantity NUMERIC(20,8) NOT NULL,
 quantity_remaining NUMERIC(20,8),
 limit_value NUMERIC(20,8),
 price NUMERIC(20,8),
 open_at VARCHAR(255),
 close_at VARCHAR(255),
 reserved NUMERIC(20,8),
 reserved_remaining NUMERIC(20,8)
)
WITH (OIDS=FALSE)
;

DROP TABLE IF EXISTS "payment";
CREATE TABLE "payment" (
 payment_id uuid NOT NULL,
 user_id int4 NOT NULL,
 currency VARCHAR(10) NOT NULL,
 amount NUMERIC(20,8) NOT NULL,
 address VARCHAR(255) NOT NULL,
 open_at VARCHAR(255),
 side VARCHAR(255),
 pending_payment NUMERIC(20,8),
 txcost NUMERIC(20,8),
 txid VARCHAR(255),
 cancelled BOOLEAN DEFAULT false
)
WITH (OIDS = FALSE);

DROP TABLE IF EXISTS "wallet";
CREATE TABLE "wallet" (
 wallet_id uuid NOT NULL,
 user_id int4,
 market VARCHAR(20) NOT NULL,
 privacy_key VARCHAR (255) NOT NULL,
 public_key VARCHAR (255) NOT NULL,
 volumn NUMERIC(20,8)
)
WITH (OIDS = FALSE);

DROP TABLE IF EXISTS "market";
CREATE TABLE "market" (
 market VARCHAR(20),
 high NUMERIC(20,8),
 low NUMERIC(20,8),
 lastp NUMERIC(20,8),
 volumn NUMERIC(20,8),
 bid NUMERIC(20,8),
 ask NUMERIC(20,8),
 txfee NUMERIC(20,8),
 is_active BOOLEAN DEFAULT true
)
WITH (OIDS = FALSE);

DROP TABLE IF EXISTS "currency";
CREATE TABLE "currency" (
 market VARCHAR(20),
 high NUMERIC(20,8),
 low NUMERIC(20,8),
 lastp NUMERIC(20,8),
 volumn NUMERIC(20,8),
 bid NUMERIC(20,8),
 ask NUMERIC(20,8),
 txfee NUMERIC(20,8),
 is_active BOOLEAN DEFAULT true
)
WITH (OIDS = FALSE);

DROP TABLE IF EXISTS "sessions";
CREATE TABLE "sessions" (
"id" varchar(255) COLLATE "default" NOT NULL,
"user_id" int4,
"created_at" varchar(255) COLLATE "default",
"expired_at" varchar(255) COLLATE "default"
)
WITH (OIDS=FALSE)

;



